/**
 * Created by Hervy on 5/7/2017.
 */
import * as Actions from "./Actions/Actions";
import GoogleMapsComponent from "./GoogleMaps";
import React from "react";
import Store from './Stores/Store'
import "./css/skeleton.css";



export default class AddNewComponent extends React.Component{
    constructor(){
        super();
        this.handleSubmit = this.handleSubmit.bind(this);
        this.getFromApi = this.getFromApi.bind(this);
        this.clearOnSuccess = this.clearOnSuccess.bind(this);
        this.preventNull = this.preventNull.bind(this);
    }

    render() {
        return(
            <div className="column">
                <form ref="form" onSubmit={this.handleSubmit}>
                    <section className="six columns">
                        <div className="row">
                            <div className="one columns">
                                <label>Name: </label>
                                <input className="u-full-width" required type="text"  ref="name"/>
                            </div>
                            <div className="one columns">
                                <label>Age: </label>
                                <input className="u-full-width" required pattern="\d*" type="text"  ref="age" placeholder="e.g: 20" />
                            </div>
                            <div className="one columns">
                                <label>Gender</label>
                                <select className="u-full-width" type="text" ref="gender">
                                    <option value="F">F</option>
                                    <option value="M">M</option>
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <label>Location:</label>
                            <input className="u-full-width" disabled type="text" value={Store.getLocationFromStore()}/>
                        </div>
                            <div className="row">
                                <GoogleMapsComponent />
                            </div>
                    </section>
                    <div className="six columns">
                        <div className="row">
                            <div className="row one columns">
                                <label>Water: </label>
                                <input className="u-full-width" pattern="\d*" type="text"  ref="water" />
                                <label>Food: </label>
                                <input className="u-full-width" pattern="\d*" type="text"  ref="food" />
                            </div>
                            <div className="row one columns">
                                <label>Medicine:</label>
                                <input className="u-full-width" pattern="\d*" type="text" ref="medicine" />
                                <label>Ammunition:</label>
                                <input className="u-full-width" pattern="\d*" type="text" ref="ammo" />
                            </div>
                            <div className="offset-by-two columns u-space-top ">
                                <input className="button-primary" type="submit"  value="Add" />
                            </div>
                        </div>
                    </div>


                </form>
            </div>
        )
    }

//Custom Functions
    /**
     * handles the submit, gathering the data from the fields as soon as the event 'e' is triggered
     * filling an object and sending as an action
     * @param e
     */
    handleSubmit(e){
        e.preventDefault();
        let addNew = {
            name: this.refs.name.value,
            age: this.refs.age.value,
            gender: this.refs.gender.value,
            location: Store.getLocationFromStore().replace(',',' '),
            items: 'Water:'+ this.preventNull(this.refs.water.value) + ';' +
            'Food:'+ this.preventNull(this.refs.food.value) + ';' +
            'Medicine:' + this.preventNull(this.refs.medicine.value) + ';' +
            'Ammunition:' + this.preventNull(this.refs.ammo.value),
        };
        Actions.postPeople(addNew);
        setTimeout(this.clearOnSuccess, 1000);
    }

    /**
     * clear the form if the new addition was a sucess.
     */
    clearOnSuccess(){
        if(+Store.getMessageFromStore().code === 201){
            this.refs.form.reset();
        }
    }

    /**
     * prevent the item fields to be passed as null, returning 0 quantity if they do.
     * @param string
     * @returns {number}
     */
    preventNull(string){
        return string ==='' ? 0 : string;
    }

    /**
     * Fetch the stats for the reports
     */
    getFromApi(){
        Actions.fetchHealthy();
        Actions.fetchInfected();
        Actions.fetchPointsLost();
        Actions.fetchItemAverage();
    }
    //----------------------------------------------------------------------------------------------------

    //LifeCycle
    componentWillMount(){
        this.getFromApi();
    }


};

