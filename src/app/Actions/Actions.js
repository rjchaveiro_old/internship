/**
 * Created by Hervy on 5/16/2017.
 */
import ActionTypes from './ActionTypes';
import Dispatcher from '../Dispatchers/Dispatcher';
import Request from 'superagent';


/**
 * Visible function to fetch data from the API
 */
export function fetchHealthy() {
    Dispatcher.dispatch({
        type: ActionTypes.FETCH_HEALTHY,
        item: Request.get("https://zssn-backend-example.herokuapp.com/api/report/non_infected.json").then((Response) => {
            return Response;
        }),
    });
}
/**
 * Visible function to fetch data from the API
 */
export function fetchInfected() {
    Dispatcher.dispatch({
        type: ActionTypes.FETCH_INFECTED,
        item: Request.get("https://zssn-backend-example.herokuapp.com/api/report/infected.json").then((Response) => {
            return Response;
        }),
    });
}
/**
 * Visible function to fetch data from the API
 */
export function fetchPointsLost() {
    Dispatcher.dispatch({
        type: ActionTypes.FETCH_POINTS_LOST,
        item: Request.get("https://zssn-backend-example.herokuapp.com/api/report/infected_points.json").then((Response) => {
            return Response;
        }),
    });
}
/**
 * Visible function to fetch data from the API
 */
export function fetchItemAverage(){
    Dispatcher.dispatch({
        type: ActionTypes.FETCH_ITEM_AVERAGE,
        item: Request.get("https://zssn-backend-example.herokuapp.com/api/report/people_inventory.json").then((Response)=>{
            return Response;
        }),
    });
}
/**
 * Visible function to fetch data from the API
 */
export function fetchPeopleById(uuid) {
    Dispatcher.dispatch({
        type: ActionTypes.FETCH_PEOPLE,
        item: Request.get("https://zssn-backend-example.herokuapp.com/api/people/"+ uuid +".json").then((Response) => {
            console.log(Response);
            return Response;
        }),
    });
}
/**
 * Visible function to add data to the API
 */
export function postPeople(item){
    Request.post("https://zssn-backend-example.herokuapp.com/api/people.json")
        .set("Content-Type","application/x-www-form-urlencoded")
        .accept("application/json")
        .send(
            "person[name]=" + item.name  +
            "&person[age]=" + item.age +
            "&person[gender]=" + item.gender +
            "&person[lonlat]=" + item.location +
            "&items=" + item.items)
        .end((err, res)=>{
            if (res.status === 0) {
                Dispatcher.dispatch({
                    type: ActionTypes.CREATE_NEW,
                    item: {code: res.status, msg: res.header.error}
                });
            }
            if(err){
                Dispatcher.dispatch({
                    type: ActionTypes.CREATE_NEW,
                    item: {code: err.status, msg: 'Name ' + res.body.name},
                });

            }else{
                Dispatcher.dispatch({
                    type: ActionTypes.CREATE_NEW,
                    item: {code: res.status, msg: 'Sucess! your survivor ID is: ' + res.body.id},
                });
            }
        }) ;
}
/**
 * Visible function to update data on the API
 */
export function patchPeople(item){
    console.log(item.id);
    Request.patch("https://zssn-backend-example.herokuapp.com/api/people/"+item.id+".json")
        .set("Content-Type","application/x-www-form-urlencoded")
        .accept("application/json")
        .send(
            "person[name]=" + item.name  +
            "&person[age]=" + item.age +
            "&person[gender]=" + item.gender +
            "&person[lonlat]=" + item.location)
        .end((err, res)=>{
            if (res.status === 0) {
                Dispatcher.dispatch({
                    type: ActionTypes.CREATE_NEW,
                    item: {code: res.status, msg: res.header.error}
                });
            }
            if(err){
                Dispatcher.dispatch({
                    type: ActionTypes.CREATE_NEW,
                    item: {code: err.status, msg: 'Name ' + res.body.name},
                });

            }else{
                Dispatcher.dispatch({
                    type: ActionTypes.CREATE_NEW,
                    item: {code: res.status, msg: 'Sucess!'},
                });
                console.log(res);
            }
        }) ;
}

/**
 * Gather the location and serves it to the store.
 * @param item
 */
export function serveLocation(item){
    Dispatcher.dispatch({
        type: ActionTypes.SERVE_LOCATION,
        item,
    });
}
