/**
 * Created by Hervy on 5/7/2017.
 */
import * as Actions from "./Actions/Actions";
import GoogleMapsComponent from "./GoogleMaps";
import React from "react";
import Store from "./Stores/Store";
import "./css/skeleton.css";


export default class UpdateOneComponent extends React.Component{
    constructor(){
        super();
        this.renderSearch = this.renderSearch.bind(this);
        this.handleSearch = this.handleSearch.bind(this);
        this.handleUpdate = this.handleUpdate.bind(this);
        this.getFromApi = this.getFromApi.bind(this);
        this.clearOnSuccess = this.clearOnSuccess.bind(this);
        this.resetRender = this.resetRender.bind(this);
        this.state = {
            toUpdate: null,
            rerender: false
        };
    }

    render() {
        if (this.state.rerender === false) {
            return (
                this.renderSearch()
            )
        }else{
            return(
                this.renderForm()
            )
        }
    }


//Custom Functions
    /**
     * Renders the HTML for the search bar and button.
     * @returns {XML}
     */
    renderSearch() {
        return (
            <div className="six columns">
                <form ref="form1" onSubmit={this.handleSearch}>
                    <section className="offset-by-five column">
                        <div className="row">
                            <div className="u-full-width">
                                <label>UUID: </label>
                                <input className="u-full-width" required type="text" ref="uuid" defaultValue={''}/>
                            </div>
                            <div className="offset-by-four column">
                                <input className="button-primary" type="submit" value="Search"/>
                            </div>
                        </div>
                    </section>
                </form>
            </div>
        );
    }

    /**
     * Execute the search
     * @param e
     */
    handleSearch(e){
        e.preventDefault();
        let uuid = this.refs.uuid.value;
        Actions.fetchPeopleById(uuid);
        setTimeout(()=>{
            this.setState({
                toUpdate: Store.getPeopleFromStore(),
                rerender: true,
            });
            this.refs.form.reset();
        },500);
    }

    /**
     * renders the HTML for the update form
     * @returns {XML}
     */
    renderForm(){
        return(
            <div className="six columns">
                <form ref="form" onSubmit={this.handleUpdate}>
                    <section className="offset-by-five columns">
                        <div className="row">
                            <div className="one columns">
                                <label>Name: </label>
                                <input className="u-full-width" required type="text"  ref="name" defaultValue={this.state.toUpdate.name}/>
                            </div>
                            <div className="one columns">
                                <label>Age: </label>
                                <input className="u-full-width" required pattern="\d*" type="text"  ref="age" placeholder="e.g: 20" defaultValue={this.state.toUpdate.age}/>
                            </div>
                            <div className="one columns">
                                <label>Gender</label>
                                <select className="u-full-width" type="text" ref="gender" >
                                    <option value="F">F</option>
                                    <option value="M">M</option>
                                </select>
                            </div>
                        </div>
                        <div className="row">
                            <span>Previous Location: {this.state.toUpdate.lonlat}</span>
                            <input className="u-full-width" disabled type="text" value={Store.getLocationFromStore()}/>
                        </div>
                        <div className="row">
                            <GoogleMapsComponent />
                        </div>
                        <div className="row offset-by-two column u-space-top">
                            <button className="button one columns u-full-width" onClick={this.resetRender} >Cancel</button>
                            <input className="button-primary one columns u-full-width" type="submit"  value="Update" />
                        </div>
                    </section>
                </form>
            </div>
        )
    }

    /**
     * handles the update, gathering the data from the fields as soon as the event 'e' is triggered
     * filling an object and sending as an action
     * @param e
     */
    handleUpdate(e){
        e.preventDefault();
        let update = {
            id: this.state.toUpdate.id,
            name: this.refs.name.value,
            age: this.refs.age.value,
            gender: this.refs.gender.value,
            location: Store.getLocationFromStore().replace(',',' '),
        };
        Actions.patchPeople(update);
        setTimeout(this.clearOnSuccess, 1000);
    }

    /**
     * resets the rendering form to being the search form only, when the update was a sucess
     */
    clearOnSuccess(){
        console.log(Store.getClonedMessageFromStore());
        if(+Store.getClonedMessageFromStore().code === 200){
          this.setState({
              rerender: false,
          });
        }

    }
    resetRender(){
        this.setState({
            rerender: false
        });
    }

    /**
     * fetch the stats for the reports
     */
    getFromApi(){
        Actions.fetchHealthy();
        Actions.fetchInfected();
        Actions.fetchPointsLost();
        Actions.fetchItemAverage();
    }
    //----------------------------------------------------------------------------------------------------

    //LifeCycle
    componentWillMount(){
        this.getFromApi();
    }

};


