/**
 * Created by Hervy on 5/16/2017.
 */
import {Dispatcher} from 'flux';

//A Dispatcher to be exported so the stores can listen to it
export default new Dispatcher;