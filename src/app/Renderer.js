/**
 * Created by Hervy on 5/10/2017.
 */
import React from 'react';
import ReactDOM from 'react-dom';
import App from './Router';


const element = document.getElementById('app-wrapper');

//render component into html
ReactDOM.render(<App />, element);
