/**
 * Created by Hervy on 5/5/2017.
 */
//Imports
import AddNewComponent from "./AddNew";
import ModalComponent from "./Modal";
import {Link} from "react-router-dom";
import React from "react";
import Store from "./Stores/Store";
import "./css/skeleton.css";


//create the main Component

export default class IndexComponent extends React.Component{
    constructor(){
        super();
        //binding the functions to this
        this.getPeople = this.getPeople.bind(this);
        //opening state
        this.state = {
            healthy_survivors: '',
            infected_survivors: '',
            points_lost:'',
            item_Average: '',
        }
    }


    render() {
        return(
            <div className="container">
                <h3>Zombie Survival Social Network</h3>
                <div className="u-full-width">
                    <Link className="a" to="/update">Update Survivor</Link>
                </div>
                <section>
                    <label className="u-pull-right">Infected Survivors</label>
                    <div className="bar-container-infected white-text u-pull-right" style={{width:(+this.state.infected_survivors * 100).toPrecision(4)+"%"}}>
                        {(+this.state.infected_survivors * 100).toPrecision(4)}%
                    </div>
                    <label>Healthy survivors</label>
                    <div className="bar-light-gray">
                        <div className="bar-container white-text" style={{width:(+this.state.healthy_survivors * 100).toPrecision(4)+"%"}}>
                            {(+this.state.healthy_survivors * 100).toPrecision(4)}%
                        </div>
                    </div>
                    <div>
                        <label>Total Points lost due to Infected: {+this.state.points_lost}</label>
                        <label >Average items per healthy person: {(+this.state.item_Average).toPrecision(4)}</label>
                    </div>
                </section>
                <div className="container u-border-top">
                    <h5>Register New Survivor</h5>
                    <ModalComponent />
                    <AddNewComponent/>
                </div>
            </div>
        );
    }

    // Custom functions
    /**
     * Handles the update on the state to update the message gathered from the store.
     */
    getPeople(){
        this.setState({
            healthy_survivors: Store.getHealthyFromStore(),
            infected_survivors: Store.getInfectedFromStore(),
            points_lost: Store.getPointsLostFromStore(),
            item_Average: Store.getItemAverageFromStore(),
        });
    }
    //----------------------------------------------------------------------------------------------------

    //lifecycle
    componentWillMount(){
        Store.on("change", this.getPeople);
    }

    componentWillUnmount(){
        Store.removeListener("change", this.getPeople);
    }

};
