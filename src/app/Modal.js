/**
 * Created by Hervy on 5/7/2017.
 */
import React from 'react';
import ReactModal from "react-modal";
import Store from './Stores/Store';
import './css/skeleton.css';


export default class ModalComponent extends React.Component {
    constructor () {
        super();
        this.state = {
            showModal: false,
            modalMessage: '',
        };
        this.handleOpenModal = this.handleOpenModal.bind(this);
        this.handleCloseModal = this.handleCloseModal.bind(this);
    }
    render () {
        return (
            <div>
                <ReactModal
                    isOpen={this.state.showModal}
                    contentLabel="Minimal Modal Example"
                    className="modal-inner"
                >
                    <div>
                        <h5>{this.state.modalMessage}</h5>
                        <button className="modal-buttons" onClick={this.handleCloseModal}>Close</button>
                    </div>
                </ReactModal>
            </div>
        );
    }

    //custom functions
    /**
     * Shows the modal only when there's content to show.
     */
    handleOpenModal () {
        let message = Store.getMessageFromStore();
        if(message !== '') {
            this.setState({
                modalMessage: message.msg,
                showModal: true
            });
        }
    }

    /**
     * closes the modal when the button is hit
     */
    handleCloseModal () {
        this.setState({
            showModal: false,
            modalMessage: ''
        });
    }
    //----------------------------------------------------------------------------------------------------

    //LifeCycle
    componentWillMount(){
        Store.on("change", this.handleOpenModal);
    }
    componentWillUnmount(){
        Store.removeListener("change", this.handleOpenModal);
    }

}


