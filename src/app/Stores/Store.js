/**
 * Created by Hervy on 5/16/2017.
 */
import ActionTypes from '../Actions/ActionTypes';
import {EventEmitter} from 'events';
import Dispatcher from '../Dispatchers/Dispatcher';

class Store extends EventEmitter{
    constructor(){
        super();
        this.message = '';
        this.healthy_survivors = null ;
        this.infected_survivors = null;
        this.points_lost = null;
        this.location = '';
        this.item_Average = null;
        this.people = null;
        this.cloned_message = '';
    }

    /**
     * @returns {Object|people}
     */
    getPeopleFromStore(){
        return this.people;
    }

    /**
     * @returns {string|message}
     */
    getMessageFromStore(){
        let auxMessage = this.message;
        this.message = '';
        return auxMessage;
    }

    /**
     * utilized to prevent the modal to re opening when rendering components
     * @returns {string|cloned_message}
     */
    getClonedMessageFromStore(){
        return this.cloned_message;
    }

    /**
     * @returns {string|healthy_survivors}
     */
    getHealthyFromStore(){
        return this.healthy_survivors;
    }

    /**
     * @returns {string|healthy_survivors}
     */
    getInfectedFromStore(){
        return this.infected_survivors;
    }

    /**
     * @returns {string|points_lost}
     */
    getPointsLostFromStore(){
        return this.points_lost;
    }

    /**
     * @returns {string|location}
     */
    getLocationFromStore(){
        return this.location;
    }
    /**
     * @returns {string|item_Average}
     */
    getItemAverageFromStore(){
        return this.item_Average;
    }


    /**
     * Adds the item object passed as parameter to the message and cloned message
     *
     * @Params Object
     * **/
    createNew(item){
        this.message = item;
        this.cloned_message = item;
        this.emit("change");
    }

    /**
     * Sets the field with the value returned inside of the item param
     * @param item
     */
    getHealthyFromServer(item){
        item.then((response) => {
            this.healthy_survivors = response.body.report.average_healthy;
            this.emit('change');
        });
    }

    /**
     * Sets the field with the value returned inside of the item param
     * @param item
     */
    getInfectedFromServer(item){
        item.then((response) => {
            this.infected_survivors = response.body.report.average_infected;
            this.emit('change');
        });
    }

    /**
     * Sets the field with the value returned inside of the item param
     * @param item
     */
    getLostPointsFromServer(item){
        item.then((response) => {
            this.points_lost = response.body.report.total_points_lost;
            this.emit('change');
        });
    }

    /**
     * Sets the field with the value returned inside of the item param
     * @param item
     */
    getPeopleFromServer(item){
        item.then((response) =>{
            this.people = response.body;
            this.emit('change');
        });
    }

    /**
     * Sets the field with the value returned inside of the item param
     * @param item
     */
    getLocationFromComponent(item){
        this.location = item;
        this.emit('change');
    }
    /**
     * Sets the field with the value returned inside of the item param
     * @param item
     */
    getAverageItemsFromServer(item){
        item.then((response) => {
            this.item_Average = response.body.report.average_items_quantity_per_healthy_person;
            this.emit('change');
        });

    }

    /**
     * Handle actions based on the type of each action it receives.
     *
     * @param action
     */
    handleActions(action){
        switch(action.type){
            case ActionTypes.CREATE_NEW: {
                this.createNew(action.item);
                break;
            }
            case ActionTypes.FETCH_HEALTHY: {
                this.getHealthyFromServer(action.item);
                break;
            }
            case ActionTypes.FETCH_PEOPLE: {
                this.getPeopleFromServer(action.item);
                break;
            }
            case ActionTypes.FETCH_INFECTED: {
                this.getInfectedFromServer(action.item);
                break;
            }
            case ActionTypes.FETCH_POINTS_LOST: {
                this.getLostPointsFromServer(action.item);
                break;
            }
            case ActionTypes.SERVE_LOCATION: {
                this.getLocationFromComponent(action.item);
                break;
            }
            case ActionTypes.FETCH_ITEM_AVERAGE: {
                this.getAverageItemsFromServer(action.item);
                break;
            }
        }
    }

}

const store = new Store();
Dispatcher.register(store.handleActions.bind(store));

export default store;