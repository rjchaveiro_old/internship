import * as Actions from './Actions/Actions';
import React from 'react';
import "./css/skeleton.css";

export default class GoogleMapsComponent extends React.Component{

    constructor() {
        super();
    }

    render(){
        return(


                <div className="google-maps" id="map" ref="map"></div>


        )
    }

    //custom Functions
    /**
     * Renders the GoogleMapsComponent from the Google maps API
     * setting the location to the current user location and zooming the map on the position.
     * Showing the message "Location not found, please pick one in the map"
     * and allowing user to place a marker on the map where it was clicked.
     * See @see {@link https://developers.google.com/maps/documentation/javascript/reference#Marker| GoogleMapsComponent API} for details
     */
    mapWithLocation(location){
        //Initial coord for google maps
        let startPos = {lat: location.coords.latitude, lng: location.coords.longitude};
        Actions.serveLocation('point('+ location.coords.latitude + ',' + location.coords.longitude+ ')');
        // google maps
        let map = new google.maps.Map(this.refs.map, {
            zoom: 16,
            center: startPos,
            streetViewControl: false,
            mapTypeControl: false,
        });
        let marker = new google.maps.Marker({
            position: startPos,
            map: map,

        });
        function placeMarker(location) {
            marker.setPosition(location);
            map.panTo(location);
        }
        google.maps.event.addListener(map, 'click', (event) => {
            placeMarker(event.latLng,map);
            Actions.serveLocation('point'+ event.latLng);
        });

    }

    /**
     * Renders the GoogleMapsComponent from the Google maps API
     * setting the location to the coords 0,0 and removing the zoom to show the world map.
     * Showing the message "Location not found, please pick one in the map"
     * and allowing user to place a marker on the map where it was clicked.
     * See @see {@link https://developers.google.com/maps/documentation/javascript/reference#Marker| GoogleMapsComponent API} for details
     */
    mapWithoutLocaiton(){
        let map = new google.maps.Map(this.refs.map, {
            center: {lat: 0, lng: 0},
            zoom: 1,
            noClear: true,
            streetViewControl: false,
            mapTypeControl: false,
        });
        Actions.serveLocation('Location not found, please pick one in the map');
        let marker = new google.maps.Marker({
            position: {lat: 0, lng: 0},
            map: map,
            opacity: 0,
        });
        function placeMarker(location) {
            marker.setPosition(location);
            marker.setOpacity(1);
            map.panTo(location);
        }
        google.maps.event.addListener(map, 'click', (event) => {
            placeMarker(event.latLng,map);
            Actions.serveLocation('point'+ event.latLng);
        });

    }
    //-------------------------------------------------------------------------------------------------------------
    componentDidMount() {
        //Mounting map with current location if possible, or map without location if current location can't be found.
        navigator.geolocation.getCurrentPosition((location) => {
            this.mapWithLocation(location);
        }, () =>{
            this.mapWithoutLocaiton();
        });
    }


}