/**
 * Created by Hervy on 5/12/2017.
 */
import UpdateIndexComponent from './UpdateIndex';
import React from 'react';
import Index from './Index';
import {BrowserRouter as Router, Route, Switch} from 'react-router-dom';

export default class App extends React.Component{
    constructor(){
        super();

    }
    render() {
        return (
            <Router history={history}>
                <Switch>
                    <Route exact={true} path={'/'} component={Index}/>
                    <Route path={'/update'} component={UpdateIndexComponent}/>
                </Switch>
            </Router>
        );
    }
};