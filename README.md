#Codeminer Internship Test
######Applicant: Rodrigo de Jesus chaveiro


>###Contents proposed that were executed
>1. Add survivors to the Database.
>1. Update Survivor Location.
>1. Survivors cannot Add/Remove items from inventory.
>1. Reports:
    * Percentage of infected survivors.
    * Percentage of non-infected survivors.
    * Average of the quantity of items per person.¹
    * Points lost because of infected survivor.


>###Notes
>¹ The requested requirement were "Average amount of each kind of resource by survivor (e.g. 5 waters per survivor)"
But said requirement is not served direct by the "reports" section of the API. Implemented where the "Average of the quantity of items per person."
There served.
>
> On the API when it concerns the location the provided format reads as follows: "point(-0000.111, 1111.000)"
however the accepted format is "point(-0000.111 1111.000)" without the comma between the coordinates
>
>Was Utilized a CSS boilerplate which the I, the applicant, weren't familiar with, deciding to follow up on this decision due to previous test
discouragement on the use of Twitter Bootstrap
